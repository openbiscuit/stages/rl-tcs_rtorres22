# RTorres2022

Internship of Richard Torres Molina (SUMO, Learning Traffic Light control)

## Context
This is the result of the work of Richard TORRES during its M2R internship, under the supervision of J. Legrand and A. Dutech.

In the 'new_approach_research' directory you will find

- latest source code of experimentation and interface with SUMO.
- final version of the report.
- final versin of the slides used during the defense of the internship.

