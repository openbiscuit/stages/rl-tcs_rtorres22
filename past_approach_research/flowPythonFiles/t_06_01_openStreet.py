
# Standard procedure
# the TestEnv environment is used to simply simulate the network
from flow.envs import TestEnv

# the Experiment class is used for running simulations
from flow.core.experiment import Experiment

# all other imports are standard
from flow.core.params import VehicleParams
from flow.core.params import NetParams
from flow.core.params import InitialConfig
from flow.core.params import EnvParams
from flow.core.params import SumoParams

# 1. Running a default simulation with OpenStreeMap
from flow.networks import Network

net_params = NetParams(
    osm_path='./bay_bridge.osm'
)

# 2. Customizing the network
# we define an EDGES_DISTRIBUTION variable with the edges within 
# the westbound Bay Bridge 

# 2.1 Specifying traversable edges
EDGES_DISTRIBUTION = [
    "11197898",
    "123741311", 
    "123741303",
    "90077193#0",
    "90077193#1", 
    "340686922", 
    "236348366", 
    "340686911#0",
    "340686911#1",
    "340686911#2",
    "340686911#3",
    "236348361", 
    "236348360#0", 
    "236348360#1"
]

# the above variable is added to initial_config
new_initial_config = InitialConfig(
    edges_distribution=EDGES_DISTRIBUTION
)

# 2.2. Creating custom routes
# create a new network class to specify the expected routes
# redefine the routes of the vehicles
class BayBridgeOSMNetwork(Network):

    def specify_routes(self, net_params):
        return {
            "11197898": [
                "11197898", "123741311", "123741303", "90077193#0", "90077193#1", 
                "340686922", "236348366", "340686911#0", "340686911#1",
                "340686911#2", "340686911#3", "236348361", "236348360#0", "236348360#1",
            ],
            "123741311": [
                "123741311", "123741303", "90077193#0", "90077193#1", "340686922", 
                "236348366", "340686911#0", "340686911#1", "340686911#2",
                "340686911#3", "236348361", "236348360#0", "236348360#1"
            ],
            "123741303": [
                "123741303", "90077193#0", "90077193#1", "340686922", "236348366",
                "340686911#0", "340686911#1", "340686911#2", "340686911#3", "236348361",
                "236348360#0", "236348360#1"
            ],
            "90077193#0": [
                "90077193#0", "90077193#1", "340686922", "236348366", "340686911#0",
                "340686911#1", "340686911#2", "340686911#3", "236348361", "236348360#0",
                "236348360#1"
            ],
            "90077193#1": [
                "90077193#1", "340686922", "236348366", "340686911#0", "340686911#1",
                "340686911#2", "340686911#3", "236348361", "236348360#0", "236348360#1"
            ],
            "340686922": [
                "340686922", "236348366", "340686911#0", "340686911#1", "340686911#2",
                "340686911#3", "236348361", "236348360#0", "236348360#1"
            ],
            "236348366": [
                "236348366", "340686911#0", "340686911#1", "340686911#2", "340686911#3",
                "236348361", "236348360#0", "236348360#1"
            ],
            "340686911#0": [
                "340686911#0", "340686911#1", "340686911#2", "340686911#3", "236348361",
                "236348360#0", "236348360#1"
            ],
            "340686911#1": [
                "340686911#1", "340686911#2", "340686911#3", "236348361", "236348360#0",
                "236348360#1"
            ],
            "340686911#2": [
                "340686911#2", "340686911#3", "236348361", "236348360#0", "236348360#1"
            ],
            "340686911#3": [
                "340686911#3", "236348361", "236348360#0", "236348360#1"
            ],
            "236348361": [
                "236348361", "236348360#0", "236348360#1"
            ],
            "236348360#0": [
                "236348360#0", "236348360#1"
            ],
            "236348360#1": [
                "236348360#1"
            ]
        }


# Create the remainding parameters
env_params = EnvParams()
sim_params = SumoParams(render=True)
initial_config = InitialConfig()
vehicles = VehicleParams()
vehicles.add('human', num_vehicles=100)


flow_params = dict(
    exp_tag='bay_bridge',
    env_name=TestEnv,
    network=BayBridgeOSMNetwork,
    simulator='traci',
    sim=sim_params,
    env=env_params,
    net=net_params,
    veh=vehicles,
    initial=new_initial_config,
)

# Experiment to test the network in the simulation
# Number of time steps
flow_params['env'].horizon = 1000
exp = Experiment(flow_params)


# run the sumo simulation
_ = exp.run(1)
