# Networks define the network geometry of the task and the constituents of the
# network such as vehicles, traffic lights, etc. 

# import Flow's base network class (base.py)
from flow.networks import Network

# import some math function for specify_edge_starts(
from numpy import pi

# some mathematical operations for specify edges
from numpy import pi, sin, cos, linspace

# 3. Testing the new network
from flow.core.params import VehicleParams
from flow.controllers import IDMController, ContinuousRouter
from flow.core.params import SumoParams, EnvParams, InitialConfig, NetParams

# Visualization
from flow.envs.ring.accel import AccelEnv, ADDITIONAL_ENV_PARAMS

# Experiment simulation
from flow.core.experiment import Experiment

# 1. Traffic Network Feautures Specification 
# 1.1 Additional net paramas
# Informaiton needed to define the network parameters
ADDITIONAL_NET_PARAMS = {
    "radius": 40,
    "num_lanes": 1,
    "speed_limit": 30,
}

# print(ADDITIONAL_NET_PARAMS)
# print(ADDITIONAL_NET_PARAMS["radius"])

# define the network class, and inherit properties from the base network class
class myNetwork(Network):
   # 1.2 SPECIFY_NODES
   def specify_nodes(self, net_params):
        # one of the elements net_params will need is a "radius" value
        r = net_params.additional_params["radius"]

        # specify the name and position (x,y) of each node
        nodes = [{"id": "bottom", "x": 0,  "y": -r},
                 {"id": "right",  "x": r,  "y": 0},
                 {"id": "top",    "x": 0,  "y": r},
                 {"id": "left",   "x": -r, "y": 0}]

        return nodes

   # 1.3 SPECIFY EDGES
   def specify_edges(self, net_params):
        r = net_params.additional_params["radius"]
        edgelen = r * pi / 2
        # this will let us control the number of lanes in the network
        lanes = net_params.additional_params["num_lanes"]
        # speed limit of vehicles in the network
        speed_limit = net_params.additional_params["speed_limit"]

        edges = [
            {
                "id": "edge0",
                "numLanes": lanes,
                "speed": speed_limit,     
                "from": "bottom", 
                "to": "right", 
                "length": edgelen,
                "shape": [(r*cos(t), r*sin(t)) for t in linspace(-pi/2, 0, 40)]
            },
            {
                "id": "edge1",
                "numLanes": lanes, 
                "speed": speed_limit,
                "from": "right",
                "to": "top",
                "length": edgelen,
                "shape": [(r*cos(t), r*sin(t)) for t in linspace(0, pi/2, 40)]
            },
            {
                "id": "edge2",
                "numLanes": lanes,
                "speed": speed_limit,
                "from": "top",
                "to": "left", 
                "length": edgelen,
                "shape": [(r*cos(t), r*sin(t)) for t in linspace(pi/2, pi, 40)]},
            {
                "id": "edge3", 
                "numLanes": lanes, 
                "speed": speed_limit,
                "from": "left", 
                "to": "bottom", 
                "length": edgelen,
                "shape": [(r*cos(t), r*sin(t)) for t in linspace(pi, 3*pi/2, 40)]
            }
        ]

        return edges


    # 1.4 SPECIFIC ROUTES
    # Multiple routes per edge
   def specify_routes(self, net_params):
        rts = {"edge0": [(["edge0", "edge1", "edge2", "edge3"], 1)],
               "edge1": [(["edge1", "edge2", "edge3", "edge0"], 1)],
               "edge2": [(["edge2", "edge3", "edge0", "edge1"], 1)],
               "edge3": [(["edge3", "edge0", "edge1", "edge2"], 1)]}
        return rts
    
  # 2. Specifying auxiliary NETWORK features
  # 2.2 Specifiying the starting position of edges
  # The tuple is the name of the edge/intersection/internal_link,
  # and the second value is the distance of the link from some global reference
   def specify_edge_starts(self):
        r = self.net_params.additional_params["radius"]

        edgestarts = [("edge0", 0),
                      ("edge1", r * 1/2 * pi),
                      ("edge2", r * pi),
                      ("edge3", r * 3/2 * pi)]

        return edgestarts

   #3. Testing the new network
vehicles = VehicleParams()

vehicles.add(veh_id="human",
             acceleration_controller=(IDMController, {}),
             routing_controller=(ContinuousRouter, {}),
             num_vehicles=22)

sim_params = SumoParams(sim_step=0.1, render=True)

initial_config = InitialConfig(bunching=40)

env_params = EnvParams(additional_params=ADDITIONAL_ENV_PARAMS)

additional_net_params = ADDITIONAL_NET_PARAMS.copy()

net_params = NetParams(additional_params=additional_net_params)

flow_params = dict(
    exp_tag='test_network',
    env_name=AccelEnv,
    network=myNetwork,
    simulator='traci',
    sim=sim_params,
    env=env_params,
    net=net_params,
    veh=vehicles,
    initial=initial_config,
)

# number of time steps
flow_params['env'].horizon = 1500
exp = Experiment(flow_params)

# run the sumo simulation
_ = exp.run(1)


