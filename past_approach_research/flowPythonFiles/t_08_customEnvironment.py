# 1. Creating an environment class
# import the base environment class
from flow.envs import Env

from gym.spaces.box import Box

from gym.spaces import Tuple

from flow.controllers import IDMController, ContinuousRouter

from flow.core.experiment import Experiment

from flow.core.params import SumoParams, EnvParams, \
    InitialConfig, NetParams
 
from flow.core.params import VehicleParams

from flow.networks.ring import RingNetwork, ADDITIONAL_NET_PARAMS

import numpy as np
#  Features to parametrize components of the state/action space 
# and the reward function

ADDITIONAL_ENV_PARAMS = {
    "max_accel": 1,
    "max_decel": 1,
}


# define the environment class, and inherit properties from the base environment class
class myEnv(Env):
   # 1.2. Defines the number and bounds of the actions provided by the RL agent
    @property
    def action_space(self):
        num_actions = self.initial_vehicles.num_rl_vehicles
        accel_ub = self.env_params.additional_params["max_accel"]
        accel_lb = - abs(self.env_params.additional_params["max_decel"])
        
        # Define a bounded array of values
        return Box(low=accel_lb,
                   high=accel_ub,
                   shape=(num_actions,))

     # 1.3. Represents the number and types of observations by the RL agent 
    @property
    def observation_space(self):
        return Box(
            low=0,
            high=float("inf"),
            shape=(2*self.initial_vehicles.num_vehicles,),
        )
        
     # 1.4.  Transforming commands specified by the RL agent into actual actions
     # Note: Review why is underscore **
    def _apply_rl_actions(self, rl_actions):
        # the names of all autonomous (RL) vehicles in the network
        rl_ids = self.k.vehicle.get_rl_ids()

        # use the base environment method to convert actions into accelerations for the rl vehicles
        self.k.vehicle.apply_acceleration(rl_ids, rl_actions)
        
      # 1.5  Extracts features from within the environments and provides as inputs to the policy 
      # Note: meaning of **!
    def get_state(self, **kwargs):
        # the get_ids() method is used to get the names of all vehicles in the network
        ids = self.k.vehicle.get_ids()

        # we use the get_absolute_position method to get the positions of all vehicles
        pos = [self.k.vehicle.get_x_by_id(veh_id) for veh_id in ids]

        # we use the get_speed method to get the velocities of all vehicles
        vel = [self.k.vehicle.get_speed(veh_id) for veh_id in ids]

        # the speeds and positions are concatenated to produce the state
        return np.concatenate((pos, vel))   
    
    # 1.6 Returns the reward associated with any given state
    def compute_reward(self, rl_actions, **kwargs):
        # get the names of all vehicles in the network
        ids = self.k.vehicle.get_ids()

        # get a list of the speeds of all vehicles in the network
        speeds = self.k.vehicle.get_speed(ids)

        # return the average of all these speeds as the reward
        return np.mean(speeds)
        
        
 
sim_params = SumoParams(sim_step=0.1, render=True)

vehicles = VehicleParams()
vehicles.add(veh_id="idm",
             acceleration_controller=(IDMController, {}),
             routing_controller=(ContinuousRouter, {}),
             num_vehicles=22)

env_params = EnvParams(additional_params=ADDITIONAL_ENV_PARAMS)

additional_net_params = ADDITIONAL_NET_PARAMS.copy()
net_params = NetParams(additional_params=additional_net_params)

initial_config = InitialConfig(bunching=20)

flow_params = dict(
    exp_tag='ring',
    # new environment
    env_name=myEnv,
    network=RingNetwork,
    simulator='traci',
    sim=sim_params,
    env=env_params,
    net=net_params,
    veh=vehicles,
    initial=initial_config,
)

# number of time steps
flow_params['env'].horizon = 1500
exp = Experiment(flow_params)

# run the sumo simulation
_ = exp.run(1)