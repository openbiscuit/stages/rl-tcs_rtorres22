# TUtorial 03: Running RLib Experiments #

# Problem to solve:
# Simulation about a single lane ring road with a single autonomous vehicle.
# The vehicle leanrs to dissipate the formation and propagation of "phantom jams"

# import flow.networks as networks
# These are the networks
# 1. print(networks.__all__)

# 1. Components of a simulation
# All simulations require two components: 1.1.) network and 1.2.) environment.
# 1.1. Networks are the features of the transportation network used in simulation.
# Networks: positions of nodes, propertities of edges, vehicles, traffic lights,
# inflows,etc.
# 1.2. Environments: Initialization, reset and advance simulations, and act as the
# primary interface between RL and the network.
# RL: environment (state/action and reward)


###$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$##########
# 2. Setting up a NETWORK #
# SIngle lane ring road
# 2.1 Setting up network parameters #
from flow.networks import RingNetwork

# input parameter classes to the network class
# 2.1 Setting up network parameters #
from flow.core.params import NetParams, InitialConfig

# network-specific parameters
# 2.1 Setting up network parameters #
from flow.networks.ring import ADDITIONAL_NET_PARAMS

# 2.2 Adding Trainable Autonomous Vehicles #
from flow.core.params import VehicleParams

# 2.2. Vehicles dynamics models: acceleration and routing behaviour
from flow.controllers import IDMController, ContinuousRouter

# 2.2. Acceleration controller to the vehicle
from flow.controllers import RLController

###$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$##########


###$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$##########
# 3. Setting up an ENVIRONMENT #
# There is the posibility to train RL agents according to different contexts
# such as autonomous vehicles or traffic lights
# The environment checks the reward, state/action spaces

# 3.1. SumoParams specifies simulation-specific variables.
from flow.core.params import SumoParams

# 3.2.  EnvParams specifies environment and experiment-specific parameters that either affect 
# the training process or the dynamics of various components within the network.
from flow.core.params import EnvParams

# 3.3. Initializing a Gym Enviroment
import flow.envs as flowenvs

# 3.3. Initializing a Gym Enviroment
from flow.envs import WaveAttenuationPOEnv

###$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$##########


######################## 4. Running RL experiments in Ray ########################

################# 4.1 Import ####################################################
## The json package is required to store the Flow experiment parameters in the params.json file, 
## as is FlowParamsEncoder #######################################################

import json

import ray
try:
    from ray.rllib.agents.agent import get_agent_class
except ImportError:
    from ray.rllib.agents.registry import get_agent_class
from ray.tune import run_experiments
from ray.tune.registry import register_env

### Create a parametrized flow environment compatible with OpenAI gym.

from flow.utils.registry import make_create_env
from flow.utils.rllib import FlowParamsEncoder


#################################################################################
# 2.1 Setting up network parameters #

# Ring road network class
network_name = RingNetwork

# name of the network
# the network classes do not need to be defined; instead users should 
# simply name the network class they wish to use
name = "training_example"

# network-specific parameters
net_params = NetParams(additional_params=ADDITIONAL_NET_PARAMS)

# initial configuration to vehicles
initial_config = InitialConfig(spacing="uniform", perturbation=1)
#####################################################################


#########################################################################
# 2.2. Adding trainable autonomous vehicles

# Vehicles class: information  of the vehicles in the network,
# wheter the vehicle is controlled by reinforment learning (RL) or not.


############## 21 vehicle without RL, human-driven vehicles ############## 
# 2.2.1. IDM model (IDMController) for acceleration behaviour,
# gaussian acceleration noise, and 0.2 m/s2 (pertubations stop-and-go behaviour).
# 2.2.2. ContinousRouter: routing controller that the vehicle may maintain their
# routes closed networks

vehicles = VehicleParams()
vehicles.add("human",
             acceleration_controller=(IDMController, {}),
             routing_controller=(ContinuousRouter, {}),
             num_vehicles=21)
             
#########################  1 vehicle wiith RL ##########################
# 2.2.1. RLController as the acceleraton controller to the vehicle # 
# An autonomous vehicle whose actions are done by an RL agent # 
vehicles.add(veh_id="rl",
             acceleration_controller=(RLController, {}),
             routing_controller=(ContinuousRouter, {}),
             num_vehicles=1) 
#########################################################################


#################### 3. Setting up the ENVIRONMENT ######################
##### The environemnt is composed by three components: 1) SumoParams,
##### 2) EnvParams and 3) Network #######################################

##### The SumoParams varriables include the lenght of any simulation step ####
#### and wheter to render the GUI when running the experiment ################
##### The parameters are a simulation step lenght of 0.1s and the GUI is #####
##### deactivated ############################################################

################### 3.1 SumoParams #####################################
############## SumoParams specifies simulation-specific variables ######
sim_params = SumoParams(sim_step=0.1, render=False)
 

################## 3.2. EnvParams ###################################### 
#### EnvParams specifies environment and experiment-specific parameters that
### either affect 1) training process or 2) the dynamics of various components
### within the network.


#### Define horizon as a variable to ensure consistent use across notebook ###
HORIZON=100

env_params = EnvParams(
    # length of one rollout
    horizon=HORIZON,

    additional_params={
        # maximum acceleration of autonomous vehicles
        "max_accel": 1,
        # maximum deceleration of autonomous vehicles
        "max_decel": 1,
        # bounds on the ranges of ring road lengths the autonomous vehicle 
        # is trained on
        "ring_length": [220, 270],
    },
)


###################################################################################
### WaveAttenuationPOEnv ##########################################################
### 3.2.  These parameters are used for bounds on the 1) accelerations on the #####
### autononomous vehicles and the 2) range of ring lenghts the agent is trained on.
### 3.3.  Train autonomous vehicles to attenuate the formation and propagation of
### waves in a partially observable variable density RING road

#### 3.3 INITIALIZING GYM ENVIRONMENT ##########################################
# flow enviroments 
# print(flowenvs.__all__)
env_name = WaveAttenuationPOEnv

##################### 3.4 SETTING UP FLOW PARAMETES ###################################
# flow_params contains the variables required by the utility function make_create_emv ##

# Creating flow_params. Make sure the dictionary keys are as specified. 
flow_params = dict(
    # name of the experiment
    exp_tag=name,
    # name of the flow environment the experiment is running on
    env_name=env_name,
    # name of the network class the experiment uses
    network=network_name,
    # simulator that is used by the experiment
    simulator='traci',
    # simulation-related parameters
    sim=sim_params,
    # environment related parameters (see flow.core.params.EnvParams)
    env=env_params,
    # network-related parameters (see flow.core.params.NetParams and
    # the network's documentation or ADDITIONAL_NET_PARAMS component)
    net=net_params,
    # vehicles to be placed in the network at the start of a rollout 
    # (see flow.core.vehicles.Vehicles)
    veh=vehicles,
    # (optional) parameters affecting the positioning of vehicles upon 
    # initialization/reset (see flow.core.params.InitialConfig)
    initial=initial_config
)

#######################################################################
####### 4. RUNNING RL EXPERIMENTS IN RAY ##############################


############ 4.2 Initializing RAY ####################################
# Number of parallel workers
N_CPUS = 2
#  Number of rollous per training iteration
N_ROLLOUTS = 1

ray.init(num_cpus=N_CPUS)

#4.3 Configuration and set up

# The algorithm or model to train. This may refer to "
#      "the name of a built-on algorithm (e.g. RLLib's DQN "
#      "or PPO), or a user-defined trainable function or "
#      "class registered in the tune registry.")

alg_run = "PPO"

agent_cls = get_agent_class(alg_run)
config = agent_cls._default_config.copy()
config["num_workers"] = N_CPUS - 1  # number of parallel workers
config["train_batch_size"] = HORIZON * N_ROLLOUTS  # batch size
config["gamma"] = 0.999  # discount rate
config["model"].update({"fcnet_hiddens": [16, 16]})  # size of hidden layers in network
config["use_gae"] = True  # using generalized advantage estimation
config["lambda"] = 0.97  
config["sgd_minibatch_size"] = min(16 * 1024, config["train_batch_size"])  # stochastic gradient descent
config["kl_target"] = 0.02  # target KL divergence
config["num_sgd_iter"] = 10  # number of SGD iterations (stochastic gradient descent)
config["horizon"] = HORIZON  # rollout horizon

# save the flow params for replay
flow_json = json.dumps(flow_params, cls=FlowParamsEncoder, sort_keys=True,
                       indent=4)  # generating a string version of flow_params
config['env_config']['flow_params'] = flow_json  # adding the flow_params to config dict
config['env_config']['run'] = alg_run

# Call the utility function make_create_env to be able to 
# register the Flow env for this experiment
create_env, gym_name = make_create_env(params=flow_params, version=0)

# Register as rllib env with Gym
register_env(gym_name, create_env)

# 4.4 Running EXPERIMENTS
# 4.6 Restart from a checkpoint 
trials = run_experiments({
    flow_params["exp_tag"]: {
        "run": alg_run,
        "env": gym_name,
        "config": {
            **config
        },
        "restore": "/ray_results/experiment/dir/checkpoint_50/checkpoint-50",
        "checkpoint_freq": 1,  # number of iterations between checkpoints
        "checkpoint_at_end": True,  # generate a checkpoint at the end
        "max_failures": 999,
        "stop": {  # stopping conditions
            "training_iteration": 1,  # number of iterations to stop after
        },
    },
})
