from flow.networks import MergeNetwork
from flow.core.params import VehicleParams
from flow.controllers import IDMController
from flow.core.params import SumoCarFollowingParams
from flow.core.params import InFlows

from flow.networks.merge import ADDITIONAL_NET_PARAMS
from flow.core.params import NetParams

from flow.core.params import SumoParams, EnvParams, InitialConfig
from flow.envs.ring.accel import AccelEnv, ADDITIONAL_ENV_PARAMS
from flow.core.experiment import Experiment


# 1. Creating inflows in Flow
# create an empty vehicles object
vehicles = VehicleParams()

# add some vehicles to this object of type "human"
vehicles.add("human",
             acceleration_controller=(IDMController, {}),
             car_following_params=SumoCarFollowingParams(
                 speed_mode="obey_safe_speed",  
                 # the speed mode "obey_safe_speed" for better dynamics at the merge
             ),
             num_vehicles=20)

inflow = InFlows()


inflow.add(veh_type="human",
           edge="inflow_highway",
           vehs_per_hour=2000)
           

inflow.add(veh_type="human",
           edge="inflow_merge",
           vehs_per_hour=100)

# 2. Running simulations with inflows

additional_net_params = ADDITIONAL_NET_PARAMS.copy()
# make the part of the highway after the merge longer
additional_net_params['post_merge_length'] = 350  
# make the number of lanes on the highway be just one
additional_net_params['highway_lanes'] = 1

net_params = NetParams(inflows=inflow,  # our inflows
                       additional_params=additional_net_params)
                       
                       
sim_params = SumoParams(render=True,
                         sim_step=0.2)

env_params = EnvParams(additional_params=ADDITIONAL_ENV_PARAMS)

initial_config = InitialConfig()

flow_params = dict(
    exp_tag='merge-example',
    env_name=AccelEnv,
    network=MergeNetwork,
    simulator='traci',
    sim=sim_params,
    env=env_params,
    net=net_params,
    veh=vehicles,
    initial=initial_config,
)

# number of time steps
flow_params['env'].horizon = 10000
exp = Experiment(flow_params)

# run the sumo simulation
_ = exp.run(1)
                       



