# 2. Setting up the network
from flow.networks.ring import RingNetwork
# Stores information on all vehicles in the network
from flow.core.params import VehicleParams
#  Accelreratiion behaviour of all vehicles
from flow.controllers.car_following_models import IDMController
# Routing behaviour: Controller is used to perpetually reroute all vehicles to the initial set route
from flow.controllers.routing_controllers import ContinuousRouter

# These are network-specific parameters used to define the shape and properties of a network
from flow.networks.ring import ADDITIONAL_NET_PARAMS

# Other inputs may be needed from NetParams to recreate proper network features/behaviour
from flow.core.params import NetParams

# Specifies parameters that affect the positioning of vehicle in the network
# at the start of a simulation
from flow.core.params import InitialConfig

# Used to describe the positions and types of traffic lights in the network
from flow.core.params import TrafficLightParams

# 3. Setting up an environment #
# Environment used to trin a variable number of vehicles in a fully
# observable network with a static number of vehicles
from flow.envs.ring.accel import AccelEnv

# Specifies simulation-specific variables
from flow.core.params import SumoParams
from flow.envs.ring.accel import ADDITIONAL_ENV_PARAMS

# Specifies environment and experiment-specific parameters that either affect the
# training process or the dynamic of various components iwithin the network
from flow.core.params import EnvParams

# 4. Experiment object
from flow.core.experiment import Experiment


# 5. Visualizing post-simulation
import os

# Transfer to a .csv file

import pandas as pd


# Name by the parameters in Flow
name = "ring_example"

# Empty vehicle parameter object
vehicles = VehicleParams()

# Add 22 vehicles of type "human" with the above acceleration and routing behaviour
vehicles.add("human",
             acceleration_controller=(IDMController, {}),
             routing_controller=(ContinuousRouter, {}),
             num_vehicles=22)


# requirements are: length, lanes, speed and resolution
print(ADDITIONAL_NET_PARAMS)


# No attributes are needed aside from the additional_params terms
net_params = NetParams(additional_params=ADDITIONAL_NET_PARAMS)

# To introduce a small perturbation term in Initial Config to 1m
initial_config = InitialConfig(spacing="uniform", perturbation=1)

# Empty TrafficLightParams object, thereby ensuring that none are placed on any nodes
traffic_lights = TrafficLightParams()

# Tutorial 04. Visualiazing Experiment Results
# Using 
# This simulation consider a step length of 0.1s and activate the GUI
# Visualization for the data collection
sim_params = SumoParams(sim_step=0.1, render=True, emission_path='data')

print(ADDITIONAL_ENV_PARAMS)


# It is added the additional_params
env_params = EnvParams(additional_params=ADDITIONAL_ENV_PARAMS)

# Experiment  parameters
flow_params = dict(
    exp_tag='ring_example',
    env_name=AccelEnv,
    network=RingNetwork,
    simulator='traci',
    sim=sim_params,
    env=env_params,
    net=net_params,
    veh=vehicles,
    initial=initial_config,
    tls=traffic_lights,
)

# number of time steps
# Create a .csv file
flow_params['env'].horizon = 3000
exp = Experiment(flow_params)

# run the sumo simulation
_ = exp.run(1, convert_to_csv=True)

# Create a .xml file
#emission_location = os.path.join(exp.env.sim_params.emission_path, exp.env.network.name)
#print(emission_location + '-emission.xml')


# A .csv file is read
#pd.read_csv(emission_location + '-emission.csv') 
