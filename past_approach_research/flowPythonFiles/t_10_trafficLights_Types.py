from flow.core.params import NetParams
from flow.networks.traffic_light_grid import TrafficLightGridNetwork
from flow.core.params import TrafficLightParams
from flow.core.params import SumoParams, EnvParams, InitialConfig, NetParams, \
    InFlows, SumoCarFollowingParams
from flow.core.params import VehicleParams
import numpy as np


# 1. New parameters in additional_net_params
inner_length = 300
long_length = 500
short_length = 300
n = 2 # rows
m = 3 # columns
num_cars_left = 20
num_cars_right = 20
num_cars_top = 20
num_cars_bot = 20
tot_cars = (num_cars_left + num_cars_right) * m \
    + (num_cars_top + num_cars_bot) * n


# passes information on the road network to the network
grid_array = {"short_length": short_length, "inner_length": inner_length,
              "long_length": long_length, "row_num": n, "col_num": m,
              "cars_left": num_cars_left, "cars_right": num_cars_right,
              "cars_top": num_cars_top, "cars_bot": num_cars_bot}
              

#  2. Defining Traffic Light Phases
# A phase is defined as the states that the traffic lights around an intersection can take. 
# More control over individual traffic lights
tl_logic = TrafficLightParams()

nodes = ["center0", "center1", "center2", "center3", "center4", "center5"]
phases = [{"duration": "31", "state": "GrGr"},
          {"duration": "6", "state": "yryr"},
          {"duration": "31", "state": "rGrG"},
          {"duration": "6", "state": "ryry"}]
          
          
for node_id in nodes:
    tl_logic.add(node_id, tls_type="static", programID="1", offset=None, phases=phases)
    
    
    
    
additional_net_params = {"grid_array": grid_array, "speed_limit": 35,
                         "horizontal_lanes": 1, "vertical_lanes": 1,
                         "traffic_lights": True}
net_params = NetParams(additional_params=additional_net_params)

# In the network is added tl_logic 
network = TrafficLightGridNetwork(name="grid",
                            vehicles=VehicleParams(),
                            net_params=net_params,
                            initial_config=InitialConfig(),
                            traffic_lights=tl_logic)
                            
                            
                            

# 3. Static Traffic Lights
# It cannot adjust according to the traffic needs
# tl_logic = TrafficLightParams(baseline=False)
# phases = [{"duration": "31", "state": "GrGr"},
 #         {"duration": "6", "state": "yryr"},
 #        {"duration": "31", "state": "rGrG"},
 #         {"duration": "6", "state": "ryry"}]
# tl_logic.add("center0", phases=phases, programID=1)


# 4. Actuated Traffic Lights
# tl_logic = TrafficLightParams(baseline=False)
# phases = [{"duration": "31", "minDur": "8", "maxDur": "45", "state": "GrGr"},
#         {"duration": "6", "minDur": "3", "maxDur": "6", "state": "yryr"},
#          {"duration": "31", "minDur": "8", "maxDur": "45", "state": "rGrG"},
#          {"duration": "6", "minDur": "3", "maxDur": "6", "state": "ryry"}]

# tl_logic.add("center1", 
#             tls_type="actuated", 
#             programID="1", 
#             phases=phases, 
#             maxGap=5.0, 
#             detectorGap=0.9, 
#             showDetectors=False)

# tl_logic.add("center2",
#            tls_type="actuated")


# 5. Actuated Baseline Traffic Lights
# tl_logic = TrafficLightParams(baseline=True)
# additional_net_params = {"grid_array": grid_array, 
 #                        "speed_limit": 35,
 #                        "horizontal_lanes": 1, 
 #                       "vertical_lanes": 1,
 #                       "traffic_lights": True, 
 #                        "tl_logic": tl_logic}







          
 