import tensorflow as tf
from tensorflow import keras
import pandas as pd
import numpy as np
from tensorflow.keras import layers
from tensorflow.keras import losses
from tensorflow.keras.optimizers import Adam

train_df = pd.read_csv('./train_traffic.csv')
# np.random.shuffle(train_df.values)

print(train_df.head())
# LAYERS
# Inputs
inputs = keras.Input(shape=(80,))
x = layers.Dense(32, activation='relu')(inputs)
x = layers.Dense(32, activation='relu')(x)

# Output
outputs = layers.Dense(2, )(x)

# MODEL CREATION
model = keras.Model(inputs=inputs, outputs=outputs, name='my_model')
new_model = model.compile(loss=losses.MeanSquaredError(), optimizer=Adam(lr=0.001, epsilon=1e-08))

# TRAINING MODEL
# From data frame to numpy
inputs = train_df.iloc[:, 0:80].to_numpy()
print(type(inputs))
targets = train_df.iloc[:, 80:82].to_numpy()
print(type(targets))
epochs = 600

for epoch in range(0, epochs):
    print("Epoch: " + str(epoch + 1))
    model.fit(inputs, targets, epochs=1, )

test_df = pd.read_csv('./test_traffic.csv')

inputTest = test_df.iloc[:, 0:80].to_numpy()
targetTest = test_df.iloc[:, 80:82].to_numpy()

outputTest = model.predict(inputTest)
mse = np.mean(np.power(targetTest - outputTest, 2), axis=1)
totalMse = mse.mean()
print("The mean square error is : " + str(totalMse))

# Epoch: 599
# 188/188 [==============================] - 0s 1ms/step - loss: 0.0406
# The mean square error is : 0.15795914234078826

# print("Prediction", new_model.predict(np.array([
#
#    [0.0,1.0,1.0,0.0,1.0,1.0,0.0,1.0,1.0,0.0,1.0,1.0,0.0,
#     1.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0,
#     0.0,1.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,1.0,0.0,1.0,1.0,
#     0.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.0,0.0,0.0,1.0,0.0,
#     1.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,
#     0.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,0.0,0.0],
#
#     [
#         0.0,0.0,1.0,0.0,0.0,1.0,0.0,1.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,
#         1.0,1.0,0.0,0.0,1.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,1.0,1.0,
#         0.0,0.0,0.0,1.0,0.0,1.0,0.0,1.0,0.0,1.0,1.0,1.0,1.0,0.0,
#         0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0,
#         0.0,1.0,0.0,0.0,1.0,1.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,
#         1.0,0.0,1.0,1.0,0.0,1.0,1.0
#     ]
# ])))
