import csv
import numpy as np
from numpy import random

fileName = './test_traffic.csv'
numberSamples = 1200


def printFile(data):
    with open(fileName, 'w', newline='') as f:
        w = csv.writer(f, delimiter=',')

        for i, row in enumerate(data):
            data = [row]
            w.writerows(data)


def createData():
    data = []
    for _ in range(0, numberSamples):
        # Column's size
        # inputs = np.random.binomial(n=1, p=0.9, size=[80])
        inputs = np.random.randint(2, size=80)
        data.append(np.concatenate((inputs, [random.random(), random.random()])))

    return data


if __name__ == '__main__':
    new_data = createData()
    printFile(new_data)
