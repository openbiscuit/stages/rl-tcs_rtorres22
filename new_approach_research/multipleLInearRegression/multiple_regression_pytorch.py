import pandas as pd
import numpy as np

import numpy as np
import torch
import torch.optim as optim
import torch.nn.functional as F
import torch.nn as nn

train_df = pd.read_csv('./train_traffic.csv')
# np.random.shuffle(train_df.values)
print(train_df.head())


class newModel(nn.Module):
    def __init__(self):
        super(newModel, self).__init__()
        self._layers = 32
        self._output_dim = 2
        self._linear_one = nn.Linear(80, self._layers)
        self._linear_two = nn.Linear(self._layers, self._layers)
        self._linear_three = nn.Linear(self._layers, self._output_dim)

    def forward(self, x):
        x = F.relu(self._linear_one(x))
        x = F.relu(self._linear_two(x))
        output = (self._linear_three(x))
        return output  # actions


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
criterion = nn.MSELoss()
model = newModel()
learning_rate = 0.001
num_epochs = 600
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# TRAINING MODEL
inputs = train_df.iloc[:, 0:80].to_numpy()
targets = train_df.iloc[:, 80:82].to_numpy()

inputs = torch.from_numpy(inputs.astype(np.float32))
targets = torch.from_numpy(targets.astype(np.float32))
inputs = torch.reshape(inputs, (-1, 80)).to(device)
targets = torch.reshape(targets, (-1, 2)).to(device)

print("Training ...")

for epoch in range(num_epochs):
    # forward feed
    y_pred = model.forward(inputs)

    # calculate the loss
    # reduce the loss and can be done by optimizer
    # optimizer is stochastic gradient descent
    loss = criterion(y_pred, targets)

    # backward propagation: calculate gradients
    loss.backward()

    # update the weights
    optimizer.step()

    # clear out the gradients from the last step loss.backward()
    optimizer.zero_grad()

    print('epoch {}, loss {}'.format(epoch, loss.item()))

# TESTING
print("Testing ...")
test_df = pd.read_csv('./test_traffic.csv')

inputs = test_df.iloc[:, 0:80].to_numpy()
targets = test_df.iloc[:, 80:82].to_numpy()

inputs = torch.from_numpy(inputs.astype(np.float32))
inputs = torch.reshape(inputs, (-1, 80)).to(device)

outputs = model.forward(inputs).detach().numpy()

mse = np.mean(np.power(targets - outputs, 2), axis=1)
totalMse = mse.mean()
print("The mean square error is : " + str(totalMse))

# epoch 599, loss 0.07441090792417526
# The mean square error is : 0.09105567203740032

# print("Prediction", new_model.predict(np.array([
#
#    [0.0,1.0,1.0,0.0,1.0,1.0,0.0,1.0,1.0,0.0,1.0,1.0,0.0,
#     1.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0,
#     0.0,1.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,1.0,0.0,1.0,1.0,
#     0.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.0,0.0,0.0,1.0,0.0,
#     1.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,
#     0.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0,0.0,1.0,0.0,0.0,0.0,0.0],
#
#     [
#         0.0,0.0,1.0,0.0,0.0,1.0,0.0,1.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,
#         1.0,1.0,0.0,0.0,1.0,1.0,1.0,1.0,0.0,1.0,1.0,1.0,1.0,1.0,
#         0.0,0.0,0.0,1.0,0.0,1.0,0.0,1.0,0.0,1.0,1.0,1.0,1.0,0.0,
#         0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0,
#         0.0,1.0,0.0,0.0,1.0,1.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,
#         1.0,0.0,1.0,1.0,0.0,1.0,1.0
#     ]
# ])))
